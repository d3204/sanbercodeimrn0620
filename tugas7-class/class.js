console.log("1. ANIMAL CLASS");
console.log("");
console.log("Release 0");
class Animal{
    constructor(name){
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
    get cname(){
        return this.name;
    }
    get clegs(){
        return this.legs;
    }
    get ccold_blooded(){
        return this.cold_blooded;
    }
}

var sheep = new Animal("shaun");
console.log(sheep.cname) // "shaun"
console.log(sheep.clegs) // 4
console.log(sheep.ccold_blooded) // false
console.log("");
console.log("Release 1");

class Ape extends Animal {
    
    constructor(name){
        super(name);
        this.bunyi = "Auooo";   
        this.legs = 2;
    }
    yell(){
        return console.log('"'+this.bunyi+'"');
    }
}
class Frog extends Animal {
    constructor(name){
        super(name);
        this.act = "hop hop";
    }
    jump(){
        return console.log('"'+this.act+'"');
    }
}
var sungokong = new Ape("kera sakti")
sungokong.yell() ;
var kodok = new Frog("buduk")
kodok.jump() ;
console.log("");
console.log("2. FUNCTION CLASS");
class Clock {
    constructor({ template }) {
      this.template = template;
    }
  
    render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    }
  }
var clock = new Clock({template: 'h:m:s'});
clock.start();  