// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
function buku(start,time){
    if(start>=books.length){
        start=0;
    }
    if(time>=books[start].timeSpent){
    readBooks(time,books[start],function(sisawaktu){
        buku(start+1,sisawaktu);
    })
}else{
    readBooks(time,books[start],function(time){
        console.log(time);
    })
}
 
}
buku(0,10000);



/*readBooks(10000, books[0], function (time) {
    readBooks(time, books[1], function (time) {
        readBooks(time, books[2], function (time) {});
    });
});*/
