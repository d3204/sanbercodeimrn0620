import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text,
    View,
    Image, 
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class VideoItem extends Component {
    render(){
        let laptop = this.props.laptop;
        return(
            <View style={styles.container}>
                <Image source={{ uri: laptop.gambaruri }} style={{height:200}}/>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container : {
        padding : 15
    },
    descContainer : {
        flexDirection : 'row',
        paddingTop: 15
    },
    videoTitle:{
        fontSize: 16,
        color:'#3c3c3c'
    },
    videoDetails:{
        paddingHorizontal: 15,
        flex : 1
    },
    videoStats:{
        fontSize: 15,
        paddingTop: 3
    }
});