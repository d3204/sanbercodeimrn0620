import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import 'react-native-gesture-handler';

//import Tugas13 from './Tugas/Tugas13/AboutScreen';
//import Tugas14 from './Tugas/Tugas14/SkillScreen';
//import Tugas15 from './Tugas/Tugas15/SkillScreen';
import Quiz3 from './Quiz3/LoginScreen';
export default function App() {
  return ( 
   <Quiz3 />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
