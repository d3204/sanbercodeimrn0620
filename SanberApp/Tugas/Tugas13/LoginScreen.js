import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { 
  StyleSheet, 
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  PixelRatio
  } from 'react-native';


export default function App() {
  return ( 
  <View style={styles.container}>
    <View style={styles.header}>
      <Image source={require('./images/logo.png')} style={{width: 375, height:102}}></Image>
     
    </View>
    <View> 
     
    <View style={styles.bodyHead}>
    <Text style={styles.bodyHead} style={{fontSize: 24, lineHeight:28}}>Register</Text></View>
    </View>  
    <View style={styles.body}>
      <Text style={styles.bodyItem}>Username</Text>
      <TextInput style={styles.bodyBox}></TextInput>
      <Text style={styles.bodyItem} >Email</Text>
      <TextInput style={styles.bodyBox}></TextInput>
      <Text style={styles.bodyItem}>Password</Text>
      <TextInput style={styles.bodyBox}></TextInput>
      <Text style={styles.bodyItem} >Ulangi Pasword</Text>
      <TextInput style={styles.bodyBox}></TextInput>
    </View>
    <View style={styles.bottom}>
    <TouchableOpacity style={styles.button}>
        <Text style={styles.buttonItem} style={{color:'white'}}>Daftar</Text>
      </TouchableOpacity>
      <Text style={styles.buttonItem} style={{color: '#3EC6FF',paddingTop:10,paddingBottom:10}}>atau</Text>
      <TouchableOpacity style={styles.button2} >
        <Text style={styles.buttonItem} style={{color:'white'}}>Masuk?</Text>
      </TouchableOpacity>
    </View>
  </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position : "absolute"
  },
  header : {
    flex : 1,
    backgroundColor : 'white',
    paddingTop: 10,
    paddingLeft :0,
    flexDirection : 'column',
    paddingBottom : 10,
    alignItems : 'center'
  },
  bodyHead : {
    flex :1,
    alignItems : 'center',
    paddingBottom : 10
  },
  body : {
    flex :1,
    paddingLeft : 40,
  },
  bodyItem : {
    alignItems : 'flex-end',
    fontFamily : 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    color : '#003366',
    fontSize : 16,
    lineHeight : 19,
    backgroundColor : '#FFFFFF',
    borderColor : '#003366',
    paddingBottom: 10,
    paddingTop :10
  },
  bodyBox : {
    height : 48,
    width : 294,
    borderWidth :1,
    borderColor :'#003366',
    paddingBottom: 10,
    paddingTop :10
  },
  bottom : {
    flex : 1,
    flexDirection : 'column',
    paddingTop : 20,
    paddingBottom : 20,
    alignItems : 'center',
    backgroundColor : 'white'
  },
  button : {
    width : 140,
    height :40,
    alignItems : 'center',
    borderRadius : 16 ,
    backgroundColor : '#003366',
    paddingTop : 10,
    alignItems : 'center'
    
  },
  button2 : {
    width : 140,
    height :40,
    alignItems : 'center',
    borderRadius : 16 ,
    backgroundColor : '#3EC6FF',
    paddingTop : 10,
    alignItems : 'center'
    
  },
  buttonItem : {
    paddingTop : 20,
    width : 67,
    height :28,
    fontSize : 24,
    lineHeight : 29,
    fontFamily : 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    
  }

});
