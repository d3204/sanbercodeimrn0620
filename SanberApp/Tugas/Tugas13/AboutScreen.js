import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { 
  StyleSheet, 
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity
  } from 'react-native';
  import Icon from 'react-native-vector-icons/FontAwesome';
import { color } from 'react-native-reanimated';


export default function App() {
  return ( 
  <View style={styles.container}>
      <View style={styles.about}>
        <Text style={styles.aboutItem}>Tentang Saya</Text>
        <Image style={styles.Image} source={require('./images/leeminho.png')}></Image>
        <Text style={styles.aboutItem} style={{fontSize : 24, lineHeight : 28, fontWeight:'bold' }}>Lee Min Ho</Text>
        <Text style={styles.aboutItem} style={{fontSize : 16,lineHeight:19, color : '#3EC6FF'}}>React Native Developer</Text>
      </View>
      <View style={styles.body}>
        <Text style={styles.bodyJudul}  style={{paddingBottom : 10}}>Portofolio</Text>
        <View style={{backgroundColor : '#003366', height: 1, marginBottom : 10}}></View>
          <View style={styles.navBody}>
            <Icon  style={styles.navBody} name="gitlab" size={50}></Icon>
            <Icon style={styles.navBody} name="github" size={50}></Icon>
          </View>
          <View style={styles.navBody}>
           <Text style={styles.navBodyText} >@d3204</Text>
            <Text style={styles.navBodyText}>@lalala</Text> 
          </View>
      </View>
      <View style={styles.bottom}>
        <Text style={styles.bodyJudul} style={{paddingBottom : 10}}>Hubungi Saya</Text>
        <View style={{backgroundColor : '#003366', height: 1, marginBottom : 10}}></View>
        <View style={styles.navBottom}>
          <Icon style={styles.navBottom}  name="facebook-square" size={50}></Icon>
          <Text style={styles.navBottomText} >@facebook</Text>
        </View>
        <View style={styles.navBottom}>
          <Icon style={styles.navBottom} name="instagram" size={50}></Icon>
          <Text style={styles.navBottomText}>@instagram</Text>
        </View>
        <View style={styles.navBottom}>
          <Icon style={styles.navBottom} name="twitter" size={50}></Icon>
          <Text style={styles.navBottomText}>@twitter</Text>
        </View>
      </View>
      <View style={styles.bottomlow}>
      </View>
  </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  about : {
    width : 340,
    height : 240,
    alignItems : 'center',
    marginLeft : 10,
    marginRight : 10,
    marginBottom : 10,
    marginTop : 30,
    backgroundColor : 'white'
  },
  aboutItem : {
    flexDirection : 'row',
    paddingTop : 30,
    paddingBottom : 10,
    fontFamily : 'Roboto',
    fontSize : 36,
    fontWeight : 'bold',
    lineHeight : 42,
    color : '#003366'
  },
  Image : {
    width : 100,
    height : 100,   
    borderRadius :25
  },
  body : {
    backgroundColor : '#EFEFEF',
    padding: 16,
    marginLeft :10,
    marginRight : 10,
    marginBottom : 10,
    width : 340,
    height : 160,
    borderRadius : 25,
    fontFamily : 'Roboto'
    
  },
  navBody:{
    paddingTop : 6,
    flexDirection:'row',
    justifyContent: 'space-around',
    color : '#3EC6FF'
  },
  navBodyText : {
    fontSize : 16,
    lineHeight : 19,
    fontWeight : 'bold',
    color : '#003366',
    paddingTop : 4

  },
  bottom : {
    width: 340,
    padding: 16,
    height : 250,
    backgroundColor : '#EFEFEF',
    borderRadius : 25,
    marginLeft :10,
    marginRight : 10,
    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'normal',
    color : '#3EC6FF',
    
    
  },
  navBottom:{
    paddingTop : 6,
    flexDirection:'row',
    justifyContent :'space-evenly',
    alignItems : 'center',
    color : '#3EC6FF'
    
  },
  navBottomText : {
    fontSize : 16,
    lineHeight : 19,
    fontWeight : 'bold',
    color : '#003366',
    paddingTop : 4

  },
  bottomlow : {
    flex:1,
    backgroundColor : 'white'
  }
  

});
