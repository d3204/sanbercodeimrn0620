import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { 
  StyleSheet, 
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  PixelRatio
  } from 'react-native';
  import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


export default function App() {
  return ( 
  <View style={styles.container}>
    <View style={styles.header}>
        <View style={styles.logo}>
            <Image source={require('./images/logo.png')} style={{width :187.5,height :51}}></Image>
        </View>
        <View style={styles.iconAccount}>
            <Icon style={{paddingTop:10}} name="account-circle" style={{color:'#3EC6FF'}} size={26}></Icon>
            <View style={styles.nama}>
                <Text style={styles.nama} style={{fontSize:12,lineHeight:14,color : '#003366'}}>{"Hai,"}</Text>
                <Text style={styles.nama} style={{fontSize:16,lineHeight:19,color : '#003366'}}>Mukhlis Hanafi</Text>
            </View>
        </View>
        <View style={styles.navText}>
            <Text style={styles.navText} >SKILL</Text>
        </View>
        <View style={{
            backgroundColor : '#3EC6FF', 
            height: 4, 
            marginBottom :10,
            marginLeft :10,
            width :343
            }}>
        </View>
    </View>
    <View styles={styles.border}>
        <Text styles={styles.borderText} >Library/Framework</Text>
    </View>
     
    
  </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header :{
    width : 375,
    height : 150,
    backgroundColor : 'white',
    paddingLeft : 0,
    paddingRight : 10,
    color : '#003366'
  },
  logo : {
    paddingRight :10,
    alignItems : 'flex-end'
  },
  iconAccount : { 
    flexDirection : 'row',
    alignItems : 'flex-start',
    paddingTop :10,
    paddingLeft : 10
  },
  nama : {
    paddingLeft : 5,
  },
  navText : {
    paddingLeft : 10,
    fontSize : 36,
    lineHeight: 42,
    color:'#003366'
  },
  body :{
    flex:1
  },
  border : {

  }

});
