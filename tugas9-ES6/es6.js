console.log("No 1. MENGUBAH FUNGSI MENJADI ARROW FUNCTION");
const golden = () => {
    console.log("this is golden!!")
}
golden();
console.log("No2. Sederhanakan menjadi object literal es6");
const newFunction = (firstName,lastName) => {
   return {
       firstName,
       lastName,
       fullName(){
           console.log(`${firstName} ${lastName}`)
           return
        }
   };
}
newFunction("William", "Imoh").fullName()
console.log("No 3. Destructuring");
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
const {firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation)
console.log("No4. Array Spreading");
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined = west.concat(east)
let combined = [...west,...east];
console.log(combined)
console.log("No5. Template Literals");
const planet = "earth"
const view = "glass"
/*var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 */
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam` ;
console.log(before) 

