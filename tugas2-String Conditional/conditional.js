// if Else
console.log("Permainan Werewolf ");
console.log("");

var nama = "Junaedi" ;
var peran = "Werewolf" ; 

if (nama == "" && peran == ""){
    console.log("//" + "input nama = '' dan peran = ''");
    console.log("Nama harus diisi!");
} else if (nama == "John" || peran == " "){
    console.log("//" + "input nama = John dan peran = '' ")
    console.log("Halo John, Pilih peranmu untuk memulai game!");
} else if (nama == "Jane" && peran == "Penyihir"){
    console.log("//" + "input nama = jane dan peran = penyihir")
    console.log("Selamat datang di Dunia Werewolf, Jane");
    console.log("Halo Penyihir Jane, Kamu dapat melihat siapa yang menjadi werewolf!");
} else if (nama =="Jenita" && peran == "Guard"){
    console.log("//" + "input nama = Jenita dan peran = Guard")
    console.log("Selamat datang di Dunia Werewolf, Jenita");
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf");
} else if (nama == "Junaedi" && peran == "Werewolf"){
    console.log("//" + "input nama = Junaedi dan peran = Werewolf")
    console.log("Selamat datang di Dunia Werewolf, Junaedi");
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
}


//Switch Case

var tanggal = 21;
var bulan = 2;
var tahun = 1945;
var namabulan = '';

if (tanggal >= 1 && tanggal <= 31){
    if (tahun >=1900 && tanggal <= 2200){
        switch (bulan) {
            case 1 : { namabulan = "Januari"; break;}
            case 2 : { namabulan = "Februari"; break;}
            case 3 : { namabulan = "Maret"; break;}
            case 4 : { namabulan = "April";break; }
            case 5 : { namabulan = "Mei"; break;}
            case 6 : { namabulan = "Juni"; break;}
            case 7 : { namabulan = "Juli"; break;}
            case 8 : { namabulan = "Agustus"; break;}
            case 9 : { namabulan = "September"; break;}
            case 10 : { namabulan = "Oktober"; break;}
            case 11 : { namabulan = "November"; break;}
            case 12 : { namabulan = "Desember"; break;}
           default : { console.log("Masukkan Bulan (1-12)");
           break;}
        }    
           
    }else{
        console.log("Masukkan Tahun (1900-2200)");
    }
}else {
    console.log("Masukkan Ulang Tanggal (1-31)");
}
console.log(tanggal + ' '+ namabulan + ' ' + tahun);
