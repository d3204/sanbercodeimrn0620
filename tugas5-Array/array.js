console.log("Soal No 1. Range");
function range(startNum, finishNum){
    var number = [];
    if(startNum == null || finishNum == null){
        number.unshift(-1);
        return number;
    }else{
        if(startNum < finishNum){
            for(var i = startNum; i <= finishNum ; i++){
                number.push(i); 
            }
            return number;
        }else{
            for(var i = startNum; i >= finishNum ; i--){
                number.push(i); 
            }
            return number;
            
        }
    }   
}
console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range()); 
console.log("Soal No2.range With Step");

function rangewithStep(startNum, finishNum, step){
    var number = [];
    if(startNum < finishNum){
        for(var i=startNum; i <= finishNum ; i+=step){
            number.push(i);
        }
        return number;
    }else{
        for(var i = startNum; i >= finishNum ; i-=step){
            number.push(i); 
        }
        return number;
    }
    
}
console.log(rangewithStep(1,10,2));
console.log(rangewithStep(11,23,3));
console.log(rangewithStep(5,2,1));
console.log(rangewithStep(29,2,4));
console.log("Soal No 3. Sum Of Range");
function sum(awalDeret,AkhirDeret,step){
    var number = [];
    var total = 0;
    if(awalDeret == null || AkhirDeret == null){
        
        return total;
    }else{
        if(step == null ){
            step = 1;
            if(awalDeret < AkhirDeret){
                for(var i=awalDeret; i <= AkhirDeret ; i+=step){
                    total+=number.push(i);
                }
                return total;
            }else{
                for(var i = awalDeret; i >= AkhirDeret ; i-=step){
                    total+=number.push(i); 
                }
                return total;
            }
           
        }else{
            if(awalDeret < AkhirDeret){
                for(var i=awalDeret; i <= AkhirDeret ; i+=step){
                    total+=number.push(i);
                }
                return total;
            }else{
                for(var i = awalDeret; i >= AkhirDeret ; i-=step){
                    total+=number.push(i); 
                }
                return total;
            }
           
        }
    }
    //return number;
}

console.log(sum(1,10));
console.log(sum(5,50,2));
console.log(sum(15,10));
console.log(sum(20,10,2));
console.log(sum(1));
console.log(sum());
console.log("Soal No 4. Array Multidimensi");
function datahandling(){
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ] ;
    for(var i = 0; i<4;i++){
        console.log("Nomor ID : " + input[i][0]);
        console.log("Nama Lengkap :" + input[i][1]);
        console.log("TTL : " + input[i][2] + " " +input[i][3] );
        console.log("Hobi : " + input[i][4]);
        console.log("");
    }
  
    
}
datahandling();
console.log("Soal No 5. Balik Kata");
function balikKata(kata){
    var kalimat = " ";
    for(var i=kata.length-1 ; i>=0; i--){
        kalimat = kalimat + kata[i]
    }
    return kalimat;

}
console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));
console.log("Soal no 6. Metode Array");
function dataHandling2(data){
    var kalimat = " ";
    var id = data.splice(0,1 );
    var nama = data.splice(0,1);
    var kota = data.splice(0,1);
    var ttl = data.splice(0,1);
    var hobi = data.splice(0,1);
    var tambahan = data.splice(1,0,"Pria","SMA International Metro");
    var namabaru = nama.push(" Elsharawy");
    //var kalimat= id.join();
    //var nm =nama.join(',');
    //var kota1 = kota.join();
    //var ttl1 = ttl.join();
    //var hobi1 = hobi.join();
    
    console.log(id);
    console.log(tambahan);
    console.log(nama);
   console.log(kota);
   
    console.log(ttl);
    console.log(hobi);
    console.log(kalimat);
    //console.log(nm + "Elsharawy");
    //console.log("Provinsi " + kota1);
    //console.log(ttl1);
    //console.log(hobi1)
    console.log(newarray);
    //return ;
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);