console.log("No1. Looping WHILE");
console.log("LOOPING PERTAMA");
var number = 1;
var num =0;
while(number <= 10){
    num =num+2;
    console.log(num + ' - I love coding');
    number++;
}
console.log("LOOPING KEDUA");
var number = 0;
var num = 22;
while(number < 10){
    num =num-2;
    console.log(num + ' - I will Become a mobile developer');
    number++;
}
console.log("");
console.log("No2. Looping FOR")
for(var deret = 1; deret <= 20 ; deret++){
    var genap = deret%2;
    var ganjil = deret%3;
    if (genap == 0){
        console.log(deret + ' - Berkualitas' );
    }else if (genap != 0 && ganjil == 0){
        console.log(deret + ' - I love Coding ');
    }else {
        console.log(deret + ' - Santai');
    }    
}
console.log("");
console.log("No3. Membuat Persegi Panjang");
var simbol = "";

for(var i = 0; i < 4; i++){
    for(var j = 8; j > 0; j--){
        simbol += "#" ;    
    }
    console.log(simbol);
    simbol= "";
    
}
console.log(""); 
console.log("No4. Membuat Tangga");
var simbol = "";

for(var i = 0; i < 1; i++){
    for(var j = 0; j < 7; j++){
        simbol += "#" ;
        console.log(simbol);   
    }
    console.log("");   
}  
console.log("No5. Papan catur");
var simbol = "";
for(var i = 0; i < 8; i++){
    for(var j = 8; j > 0; j--){
        nilai = i+j;
        // console.log(nilai);
         if(nilai%2 != 0){
             simbol += "#" ;
         }else{
             simbol += " " ;
         }
            
    }
    console.log(simbol);
    simbol= "";
    
}

