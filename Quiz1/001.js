
console.log("TEST CASE BALIKSTRING");
function balikString(kata){
    var kalimat = " ";
    for(var i=kata.length-1 ; i>=0; i--){
        kalimat = kalimat + kata[i];
    }
    return kalimat;
}
console.log(balikString("abcde")) ;
console.log(balikString("rusak")) ;
console.log(balikString("racecar")) ;
console.log(balikString("haji")) ;
console.log("TEST CASE PALINDROME");
function palindrome(str) {
    var re = /[^A-Za-z0-9]/g;
    str = str.toLowerCase().replace(re, '');
    var len = str.length;
    for (var i = 0; i < len/2; i++) {
      if (str[i] !== str[len - 1 - i]) {
          return false;
      }
    }
    return true;
   }
console.log(palindrome("kasur rusak")) ;
console.log(palindrome("haji ijah")) ;
console.log(palindrome("nabasan")) ;
console.log(palindrome("nababan")) ;
console.log(palindrome("jakarta")) ;
console.log("TEST CASE BANDINGKAN ANGKA");
function bandingkan(num1,num2){
    if(num1 > 0 && num2 > 0){
        if(num1 < num2){
            return num2;
        }else if(num1 > num2){
            return num1;
        }else{
            return -1;
        }
    }else{
        return -1;
    }
}
console.log(bandingkan(10, 15)); 
console.log(bandingkan(12, 12)); 
console.log(bandingkan(-1, 10)); 
console.log(bandingkan(112, 121));
console.log(bandingkan(1)); 
console.log(bandingkan()); 
console.log(bandingkan("15", "18")) 