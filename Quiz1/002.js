function AscendingTen(number){
    var angka = " ";
    if(number == null){
        angka = -1;
        return angka;
    }else{
        for(var i=0 ; i<1;i++){ 
            for(var j=number;j<number+10;j++){
                angka = angka + " " + j;
            }
            return angka ;
        }
    }
    
}

function DescendingTen(number){
    var angka = " ";
    if(number == null){
        angka = -1;
        return angka;
    }else{
        for(var i=0 ; i<1;i++){ 
            for(var j=number;j>number-10;j--){
                angka = angka + " " + j;
            }
            return angka ;
        }
    }
}

function ConditionalAscDesc(reference, check){
   var angka = " ";
    if(check == null){
        angka = -1;
        return angka;
    }else{
        if(check%2 == 0){
            for(var i=0 ; i<1;i++){ 
                for(var j=reference;j>reference-10;j--){
                    angka = angka + " " + j;
                }
                return angka ;
            }
        }else{
            for(var i=0 ; i<1;i++){ 
                for(var j=reference;j<reference+10;j++){
                    angka = angka + " " + j;
                }
                return angka ;
            }
        }
       
    }

}

function ularTangga(){
    var simbol = "";
    var start = 100;
    for(var i = 0; i < 10; i++){
        for(var j = 10; j >0; j--){
            simbol = simbol + " " + start;
            start--;    
        }
        console.log(simbol);
        simbol= "";
        
    }
    return simbol;
}
console.log("Ascending");
console.log(AscendingTen(1));
console.log(AscendingTen(101));
console.log(AscendingTen());
console.log("Descending");
console.log(DescendingTen(100));
console.log(DescendingTen(10));
console.log(DescendingTen());
console.log("ConditionalAscDec");
console.log(ConditionalAscDesc(20, 8)) ;
console.log(ConditionalAscDesc(81, 1)) ;
console.log(ConditionalAscDesc(31)) ;
console.log(ConditionalAscDesc());
console.log("Ular Tangga"); 
console.log(ularTangga()) ;